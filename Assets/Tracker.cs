﻿using UnityEngine;

public class Tracker : MonoBehaviour
{
    public Transform toFollow;

    void Update()
    {
        transform.LookAt(toFollow);
    }
}
