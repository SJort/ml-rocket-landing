﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;
using UnityEditor.UIElements;
using Random = UnityEngine.Random;

public class RocketAgent : Agent
{
    public float forceMultiplier = 10;
    
    public Vector3 startPosition = new Vector3(0, 14f, 0);
    public float startPositionDeviation = 2f;
    
    public float startRotationDeviation = 10f;
    public float allowedRotation = 3f;
    // allowed rotation before collision
    public float allowedRotationBC = 45f;
    
    public float allowedCollisionMagnitude = 5f;

    public Transform target;
    public Rigidbody northThruster;
    public Rigidbody eastThruster;
    public Rigidbody southThruster;
    public Rigidbody westThruster;

    public Material activatedMat;
    public Material baseMat;

    private Transform _transform;
    private Rigidbody _rigidbody;

    private float _collisionMagnitude = -1f;
    private float _prevHeight = 209323423f;

    private void Start()
    {
        _transform = GetComponent<Transform>();
        _rigidbody = GetComponent<Rigidbody>();
    }

    // time between fixedupdate() is consistent, in contrary to update()
    private void FixedUpdate()
    {
        RequestDecision();
    }

    public override void OnEpisodeBegin()
    {
        ResetAgent();
    }

    public void ResetAgent()
    {
        // generate random starting position
        float startPosX = Random.Range(-startPositionDeviation, startPositionDeviation);
        float startPosY = Random.Range(-startPositionDeviation, startPositionDeviation);
        float startPosZ = Random.Range(-startPositionDeviation, startPositionDeviation);
        Vector3 startP = startPosition + new Vector3(startPosX, startPosY, startPosZ);
        // Vector3 startP = startPosition;
        
        // generate random starting rotation
        float startRotX = Random.Range(-startRotationDeviation, startRotationDeviation);
        float startRotZ = Random.Range(-startRotationDeviation, startRotationDeviation);
        Vector3 rotation = new Vector3(startRotX, 0, startRotZ);

        // reset agent state
        _transform.transform.localPosition = startP;
        _transform.transform.rotation = Quaternion.Euler(rotation);
        ResetRb(_rigidbody);
        ResetRb(northThruster);
        ResetRb(eastThruster);
        ResetRb(southThruster);
        ResetRb(westThruster);

        // reset values
        _collisionMagnitude = -1f;
        _prevHeight = 23094234234f;
    }


    public override void CollectObservations(VectorSensor sensor)
    {
        // agent position
        sensor.AddObservation(_transform.localPosition);

        // agent horizontal velocities
        sensor.AddObservation(_rigidbody.velocity.x);
        sensor.AddObservation(_rigidbody.velocity.y);
        sensor.AddObservation(_rigidbody.velocity.z);
        
        // rotations
        float xRot = WrapAngle(_transform.localEulerAngles.x);
        float zRot = WrapAngle(_transform.localEulerAngles.z);
        sensor.AddObservation(xRot);
        sensor.AddObservation(zRot);
        
        float dist = Vector3.Distance(target.position, _transform.position);
        sensor.AddObservation(dist);
        

        // target position
        sensor.AddObservation(target.transform.localPosition);
        // Debug.Log("Passed observations: " + IntArrToStr(sensor.GetObservationShape()));
    }

    private void OnCollisionEnter(Collision col)
    {
        _collisionMagnitude = col.relativeVelocity.magnitude;

        float xRot = WrapAngle(_transform.localEulerAngles.x);
        float zRot = WrapAngle(_transform.localEulerAngles.z);
        Debug.Log("Collided with magnitude " + _collisionMagnitude + " with angles: " + xRot + " and " + zRot);
    }

    // onactionreceived is handled every fixed update
    public override void OnActionReceived(float[] vectorAction)
    {
        // received actions (size = 4)

        // Debug.Log("Actions received: " + ArrToStr(vectorAction));

        // rb.AddForce(transform.forward * thrust);
        northThruster.GetComponent<Renderer>().material = baseMat;
        eastThruster.GetComponent<Renderer>().material = baseMat;
        southThruster.GetComponent<Renderer>().material = baseMat;
        westThruster.GetComponent<Renderer>().material = baseMat;

        if (vectorAction[0] > 0)
        {
            northThruster.AddRelativeForce(Vector3.up * forceMultiplier * Time.deltaTime);
            northThruster.GetComponent<Renderer>().material = activatedMat;
        }

        if (vectorAction[1] > 0)
        {
            eastThruster.AddRelativeForce(Vector3.up * forceMultiplier * Time.deltaTime);
            eastThruster.GetComponent<Renderer>().material = activatedMat;
        }

        if (vectorAction[2] > 0)
        {
            southThruster.AddRelativeForce(Vector3.up * forceMultiplier * Time.deltaTime);
            southThruster.GetComponent<Renderer>().material = activatedMat;
        }

        if (vectorAction[3] > 0)
        {
            westThruster.AddRelativeForce(Vector3.up * forceMultiplier * Time.deltaTime);
            westThruster.GetComponent<Renderer>().material = activatedMat;
        }
        
        // EXPERIMENTAL ############################################################################################
        float xRott = WrapAngle(transform.localEulerAngles.x);
        float zRott = WrapAngle(transform.localEulerAngles.z);
        if (!(xRott > -allowedRotationBC && xRott < allowedRotationBC &&
              zRott > -allowedRotationBC && zRott < allowedRotationBC))
        {
            // Debug.Log("Early Fail: rotation too much. (x=" + xRott + ", z=" + zRott + ")");
            Debug.Log("Early Fail: rotation too much.");
            EndEpisode();
            return;
        }
        
        if (_transform.localPosition.y > _prevHeight)
        {
            Debug.Log("Early Fail: going up. (" + _transform.localPosition.y + ")");
            EndEpisode();
            return;
        }
        _prevHeight = transform.localPosition.y;
        
        // EXPERIMENTAL ############################################################################################

        // restart if past target
        if (_transform.localPosition.y < target.transform.localPosition.y)
        {
            Debug.Log("Fail: below platform. (" + _transform.localPosition.y + ")");
            EndEpisode();
            return;
        }

        if (_collisionMagnitude > -1)
        {
            if (_collisionMagnitude > allowedCollisionMagnitude)
            {
                Debug.Log("Fail: collision too hard. (" + _collisionMagnitude + ")");
                EndEpisode();
                return;
            }

            // euler angles is just the rotation values you see in the editor, and not normalized
            float xRot = WrapAngle(transform.localEulerAngles.x);
            float zRot = WrapAngle(transform.localEulerAngles.z);
            if (!(xRot > -allowedRotation && xRot < allowedRotation &&
                  zRot > -allowedRotation && zRot < allowedRotation))
            {
                Debug.Log("Fail: rotation too much. (x=" + xRot + ", z=" + zRot + ")");
                EndEpisode();
                return;
            }

            // reward if collided, collision was gentle and rotation was acceptable
            Debug.Log("Reward!");
            SetReward(1.0f);
            EndEpisode();
            return;
        }
    }


    public override void Heuristic(float[] actionsOut)
    {
        // they seem to have the previous values, so rewrite
        for (int i = 0; i < actionsOut.Length; i++)
        {
            actionsOut[i] = 0;
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            actionsOut[0] = 1;
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            actionsOut[1] = 1;
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            actionsOut[2] = 1;
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            actionsOut[3] = 1;
        }

        // all thrusters
        if (Input.GetKey(KeyCode.Space))
        {
            for (int i = 0; i < actionsOut.Length; i++)
            {
                actionsOut[i] = 1;
            }
        }

        // Debug.Log("Heuristic out: " + ArrToStr(actionsOut));
    }


    // resets rotation, rotation speed and velocity of a RigidBody
    public void ResetRb(Rigidbody rb)
    {
        rb.rotation = Quaternion.identity;
        rb.angularVelocity = Vector3.zero;
        rb.velocity = Vector3.zero;
    }


    // a function just to get the damn rotation values as seen in the editor (pass eulerrotation)
    private static float WrapAngle(float angle)
    {
        angle %= 360;
        if (angle > 180)
            return angle - 360;

        return angle;
    }
    
    public string ArrToStr(float[] arr)
    {
        string res = "[";
        foreach (float x in arr)
        {
            res += x + ", ";
        }

        res += "]";

        return res;
    }
    
    public string IntArrToStr(int[] arr)
    {
        string res = "[";
        foreach (int x in arr)
        {
            res += x + ", ";
        }

        res += "]";

        return res;
    }
}