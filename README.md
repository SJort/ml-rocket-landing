# ML Rocket Landing
Landing a simulated Falcon 9 using Machine Learning in Unity.  
Uses the [ML-Agents](https://github.com/Unity-Technologies/ml-agents) library.

### ML-Agents
Installing library:
```
pip3 install ml-agents
```
Straight to the point example:  
[link](https://github.com/Unity-Technologies/ml-agents/blob/master/docs/Learning-Environment-Create-New.md)  
Training configuration file parameters:  
[link](https://github.com/Unity-Technologies/ml-agents/blob/master/docs/Training-Configuration-File.md)

### Running
In the project root run:
```
tensorboard --logdir results --port 6006
```
and
```
mlagents-learn config/rocketagent_config.yaml --run-id=RocketAgent
```
Then click play in Unity and visit [Tensorboard](localhost:6006).

### Continue training with existing model
```
mlagents-learn config/rocketagent_config.yaml --run-id=RocketAgent2 --initialize-from=RocketAgent1
